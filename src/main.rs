extern crate termion;

use termion::{async_stdin, clear, color, cursor};
use termion::event::Key;
use termion::input::TermRead;
use termion::raw::IntoRawMode;
use std::io::{Read, Write, stdout};

fn main() {
    // init some variables
    let mut stdout = stdout().into_raw_mode().unwrap();
    let mut stdin = async_stdin().keys();
    let (width, height) = termion::terminal_size().unwrap();

    // prepare the screen
    write!(stdout, "{}{}", clear::All, cursor::Goto(1, height)).unwrap();
    stdout.flush().unwrap();

    // do some work
    loop {
        match stdin.next() {
            Some(Ok(key)) => {
                match key {
                    Key::Char('\n') => {
                        write!(stdout, "{}\r", clear::CurrentLine).unwrap();
                        stdout.flush().unwrap();
                    },
                    Key::Char(ch) => {
                        write!(stdout, "{}", ch).unwrap();
                        stdout.flush().unwrap();
                    }
                    Key::Esc => break,
                    _ => {}
                }
            }
            _ => {}
        }
    }

    // the end
}
